package dev.robingenz.capacitorjs.demo.firebaseplugins;

import android.os.Bundle;

import com.getcapacitor.BridgeActivity;
import com.getcapacitor.community.firebasecrashlytics.FirebaseCrashlyticsPlugin;

import dev.robingenz.capacitor.firebaseperformance.FirebasePerformancePlugin;
import com.getcapacitor.community.firebaseanalytics.FirebaseAnalytics;



public class MainActivity extends BridgeActivity {
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    registerPlugin(FirebaseCrashlyticsPlugin.class);
    registerPlugin(FirebasePerformancePlugin.class);
    registerPlugin(FirebaseAnalytics.class);


  }
}
