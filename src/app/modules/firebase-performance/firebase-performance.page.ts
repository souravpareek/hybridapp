import { Component } from '@angular/core';
import { FirebasePerformance } from '@robingenz/capacitor-firebase-performance';
import { FirebaseAnalytics } from "@capacitor-community/firebase-analytics";

@Component({
  selector: 'app-firebase-performance',
  templateUrl: './firebase-performance.page.html',
  styleUrls: ['./firebase-performance.page.scss'],
})


export class FirebasePerformancePage {

  
  private readonly githubUrl =
    'https://github.com/robingenz/capacitor-firebase-performance';

  constructor() {}

  public openOnGithub(): void {
    FirebaseAnalytics.initializeFirebase({
      apiKey: "AIzaSyBROjwWc9w-bIO_cEoHr1hKslPgJX0B-gM",
      authDomain: "hybrid-test-846be.firebaseapp.com",
      projectId: "hybrid-test-846be",
      storageBucket: "hybrid-test-846be.appspot.com",
      messagingSenderId: "150037174770",
      appId: "1:150037174770:web:9f9f4c607d38fc972b736b",
      measurementId: "G-NRWFB89SGS"
    });
    
    FirebaseAnalytics.getAppInstanceId();
    FirebaseAnalytics.setUserId({
      userId: "000bravvo",
    });

    FirebaseAnalytics.setUserProperty({
      name: "test",
      value: "heal",
    });
  }

  public async startTrace(): Promise<void> {
    await FirebasePerformance.startTrace({ traceName: 'test_trace' });
  }

  public async stopTrace(): Promise<void> {
    await FirebasePerformance.stopTrace({ traceName: 'test_trace' });
  }

  public async incrementMetric(): Promise<void> {
    await FirebasePerformance.incrementMetric({
      traceName: 'test_trace',
      metricName: 'item_cache_hit',
      incrementBy: 1,
    });
  }
}
