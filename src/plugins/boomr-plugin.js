import { BOOMR } from '/node_modules/boomerangjs/boomerangjs'
BOOMR.init({
    beacon_url: "https://testheal.s3.ap-south-1.amazonaws.com/tesdata/",
    ResourceTiming: {
        enabled: true,
        clearOnBeacon: true
      }
  });
  BOOMR.t_end = new Date().getTime();