(self["webpackChunkcapacitor_firebase_plugin_demo"] = self["webpackChunkcapacitor_firebase_plugin_demo"] || []).push([["src_app_modules_firebase-app_firebase-app_module_ts"],{

/***/ 16:
/*!********************************************************************************!*\
  !*** ./node_modules/@robingenz/capacitor-firebase-app/dist/esm/definitions.js ***!
  \********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);

//# sourceMappingURL=definitions.js.map

/***/ }),

/***/ 2465:
/*!**************************************************************************!*\
  !*** ./node_modules/@robingenz/capacitor-firebase-app/dist/esm/index.js ***!
  \**************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FirebaseApp": () => (/* binding */ FirebaseApp)
/* harmony export */ });
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @capacitor/core */ 8384);
/* harmony import */ var _definitions__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./definitions */ 16);

const FirebaseApp = (0,_capacitor_core__WEBPACK_IMPORTED_MODULE_0__.registerPlugin)('FirebaseApp', {
    web: () => __webpack_require__.e(/*! import() */ "node_modules_robingenz_capacitor-firebase-app_dist_esm_web_js").then(__webpack_require__.bind(__webpack_require__, /*! ./web */ 2345)).then(m => new m.FirebaseAppWeb()),
});


//# sourceMappingURL=index.js.map

/***/ }),

/***/ 6111:
/*!*********************************************************************!*\
  !*** ./src/app/modules/firebase-app/firebase-app-routing.module.ts ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FirebaseAppPageRoutingModule": () => (/* binding */ FirebaseAppPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _firebase_app_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./firebase-app.page */ 7271);




const routes = [
    {
        path: '',
        component: _firebase_app_page__WEBPACK_IMPORTED_MODULE_0__.FirebaseAppPage,
    },
];
let FirebaseAppPageRoutingModule = class FirebaseAppPageRoutingModule {
};
FirebaseAppPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], FirebaseAppPageRoutingModule);



/***/ }),

/***/ 1829:
/*!*************************************************************!*\
  !*** ./src/app/modules/firebase-app/firebase-app.module.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FirebaseAppPageModule": () => (/* binding */ FirebaseAppPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _app_shared__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @app/shared */ 1679);
/* harmony import */ var _firebase_app_routing_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./firebase-app-routing.module */ 6111);
/* harmony import */ var _firebase_app_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./firebase-app.page */ 7271);





let FirebaseAppPageModule = class FirebaseAppPageModule {
};
FirebaseAppPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        imports: [_app_shared__WEBPACK_IMPORTED_MODULE_0__.SharedModule, _firebase_app_routing_module__WEBPACK_IMPORTED_MODULE_1__.FirebaseAppPageRoutingModule],
        declarations: [_firebase_app_page__WEBPACK_IMPORTED_MODULE_2__.FirebaseAppPage],
    })
], FirebaseAppPageModule);



/***/ }),

/***/ 7271:
/*!***********************************************************!*\
  !*** ./src/app/modules/firebase-app/firebase-app.page.ts ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FirebaseAppPage": () => (/* binding */ FirebaseAppPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_firebase_app_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./firebase-app.page.html */ 2211);
/* harmony import */ var _firebase_app_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./firebase-app.page.scss */ 7075);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _robingenz_capacitor_firebase_app__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @robingenz/capacitor-firebase-app */ 2465);






let FirebaseAppPage = class FirebaseAppPage {
    constructor(platform) {
        this.platform = platform;
        this.name = '';
        this.apiKey = '';
        this.applicationId = '';
        this.databaseUrl = '';
        this.gcmSenderId = '';
        this.projectId = '';
        this.storageBucket = '';
        this.githubUrl = 'https://github.com/robingenz/capacitor-firebase-app';
    }
    ngOnInit() {
        if (!this.platform.is('capacitor')) {
            return;
        }
        _robingenz_capacitor_firebase_app__WEBPACK_IMPORTED_MODULE_2__.FirebaseApp.getName().then((result) => {
            this.name = result.name;
        });
        _robingenz_capacitor_firebase_app__WEBPACK_IMPORTED_MODULE_2__.FirebaseApp.getOptions().then((result) => {
            this.apiKey = result.apiKey;
            this.applicationId = result.applicationId;
            this.databaseUrl = result.databaseUrl;
            this.gcmSenderId = result.gcmSenderId;
            this.projectId = result.projectId;
            this.storageBucket = result.storageBucket;
        });
    }
    openOnGithub() {
        window.open(this.githubUrl, '_blank');
    }
};
FirebaseAppPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__.Platform }
];
FirebaseAppPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-firebase-app',
        template: _raw_loader_firebase_app_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_firebase_app_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], FirebaseAppPage);



/***/ }),

/***/ 7075:
/*!*************************************************************!*\
  !*** ./src/app/modules/firebase-app/firebase-app.page.scss ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJmaXJlYmFzZS1hcHAucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ 2211:
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/firebase-app/firebase-app.page.html ***!
  \***************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button defaultHref=\"home\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title>Firebase App</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-card>\r\n    <ion-card-header>\r\n      <ion-card-title>About</ion-card-title>\r\n    </ion-card-header>\r\n    <ion-card-content>\r\n      ⚡️ Capacitor plugin for Firebase App.\r\n    </ion-card-content>\r\n    <ion-row class=\"ion-no-padding\">\r\n      <ion-col>\r\n        <ion-button\r\n          fill=\"clear\"\r\n          (click)=\"openOnGithub()\"\r\n          class=\"ion-float-right\"\r\n          >GitHub</ion-button\r\n        >\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-card>\r\n  <ion-card>\r\n    <ion-card-header>\r\n      <ion-card-title>Demo</ion-card-title>\r\n    </ion-card-header>\r\n    <ion-card-content>\r\n      <ion-item>\r\n        <ion-label position=\"fixed\">Name</ion-label>\r\n        <ion-input type=\"text\" readonly [value]=\"name\"></ion-input>\r\n      </ion-item>\r\n      <ion-item>\r\n        <ion-label position=\"fixed\">API Key</ion-label>\r\n        <ion-input type=\"text\" readonly [value]=\"apiKey\"></ion-input>\r\n      </ion-item>\r\n      <ion-item>\r\n        <ion-label position=\"fixed\">Application ID</ion-label>\r\n        <ion-input type=\"text\" readonly [value]=\"applicationId\"></ion-input>\r\n      </ion-item>\r\n      <ion-item>\r\n        <ion-label position=\"fixed\">Database URL</ion-label>\r\n        <ion-input type=\"text\" readonly [value]=\"databaseUrl\"></ion-input>\r\n      </ion-item>\r\n      <ion-item>\r\n        <ion-label position=\"fixed\">GCM Sender ID</ion-label>\r\n        <ion-input type=\"text\" readonly [value]=\"gcmSenderId\"></ion-input>\r\n      </ion-item>\r\n      <ion-item>\r\n        <ion-label position=\"fixed\">Project ID</ion-label>\r\n        <ion-input type=\"text\" readonly [value]=\"projectId\"></ion-input>\r\n      </ion-item>\r\n      <ion-item>\r\n        <ion-label position=\"fixed\">Storage Bucket</ion-label>\r\n        <ion-input type=\"text\" readonly [value]=\"storageBucket\"></ion-input>\r\n      </ion-item>\r\n    </ion-card-content>\r\n  </ion-card>\r\n</ion-content>\r\n");

/***/ })

}]);
//# sourceMappingURL=src_app_modules_firebase-app_firebase-app_module_ts.js.map