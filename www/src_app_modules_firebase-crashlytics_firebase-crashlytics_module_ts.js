(self["webpackChunkcapacitor_firebase_plugin_demo"] = self["webpackChunkcapacitor_firebase_plugin_demo"] || []).push([["src_app_modules_firebase-crashlytics_firebase-crashlytics_module_ts"],{

/***/ 5864:
/*!****************************************************************************************!*\
  !*** ./node_modules/@capacitor-community/firebase-crashlytics/dist/esm/definitions.js ***!
  \****************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);

//# sourceMappingURL=definitions.js.map

/***/ }),

/***/ 426:
/*!**********************************************************************************!*\
  !*** ./node_modules/@capacitor-community/firebase-crashlytics/dist/esm/index.js ***!
  \**********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FirebaseCrashlytics": () => (/* binding */ FirebaseCrashlytics)
/* harmony export */ });
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @capacitor/core */ 8384);
/* harmony import */ var _definitions__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./definitions */ 5864);

const FirebaseCrashlytics = (0,_capacitor_core__WEBPACK_IMPORTED_MODULE_0__.registerPlugin)('FirebaseCrashlytics', {
    web: () => __webpack_require__.e(/*! import() */ "node_modules_capacitor-community_firebase-crashlytics_dist_esm_web_js").then(__webpack_require__.bind(__webpack_require__, /*! ./web */ 4168)).then(m => new m.FirebaseCrashlyticsWeb()),
});


//# sourceMappingURL=index.js.map

/***/ }),

/***/ 7292:
/*!*************************************************************************************!*\
  !*** ./src/app/modules/firebase-crashlytics/firebase-crashlytics-routing.module.ts ***!
  \*************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FirebaseCrashlyticsPageRoutingModule": () => (/* binding */ FirebaseCrashlyticsPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _firebase_crashlytics_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./firebase-crashlytics.page */ 5487);




const routes = [
    {
        path: '',
        component: _firebase_crashlytics_page__WEBPACK_IMPORTED_MODULE_0__.FirebaseCrashlyticsPage,
    },
];
let FirebaseCrashlyticsPageRoutingModule = class FirebaseCrashlyticsPageRoutingModule {
};
FirebaseCrashlyticsPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], FirebaseCrashlyticsPageRoutingModule);



/***/ }),

/***/ 7792:
/*!*****************************************************************************!*\
  !*** ./src/app/modules/firebase-crashlytics/firebase-crashlytics.module.ts ***!
  \*****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FirebaseCrashlyticsPageModule": () => (/* binding */ FirebaseCrashlyticsPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _app_shared__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @app/shared */ 1679);
/* harmony import */ var _firebase_crashlytics_routing_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./firebase-crashlytics-routing.module */ 7292);
/* harmony import */ var _firebase_crashlytics_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./firebase-crashlytics.page */ 5487);





let FirebaseCrashlyticsPageModule = class FirebaseCrashlyticsPageModule {
};
FirebaseCrashlyticsPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        imports: [_app_shared__WEBPACK_IMPORTED_MODULE_0__.SharedModule, _firebase_crashlytics_routing_module__WEBPACK_IMPORTED_MODULE_1__.FirebaseCrashlyticsPageRoutingModule],
        declarations: [_firebase_crashlytics_page__WEBPACK_IMPORTED_MODULE_2__.FirebaseCrashlyticsPage],
    })
], FirebaseCrashlyticsPageModule);



/***/ }),

/***/ 5487:
/*!***************************************************************************!*\
  !*** ./src/app/modules/firebase-crashlytics/firebase-crashlytics.page.ts ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FirebaseCrashlyticsPage": () => (/* binding */ FirebaseCrashlyticsPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_firebase_crashlytics_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./firebase-crashlytics.page.html */ 9232);
/* harmony import */ var _firebase_crashlytics_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./firebase-crashlytics.page.scss */ 400);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _capacitor_community_firebase_crashlytics__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @capacitor-community/firebase-crashlytics */ 426);





let FirebaseCrashlyticsPage = class FirebaseCrashlyticsPage {
    constructor() {
        this.githubUrl = 'https://github.com/capacitor-community/firebase-crashlytics';
    }
    openOnGithub() {
        window.open(this.githubUrl, '_blank');
    }
    crash() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            yield _capacitor_community_firebase_crashlytics__WEBPACK_IMPORTED_MODULE_2__.FirebaseCrashlytics.crash({ message: 'Test' });
        });
    }
};
FirebaseCrashlyticsPage.ctorParameters = () => [];
FirebaseCrashlyticsPage = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Component)({
        selector: 'app-firebase-crashlytics',
        template: _raw_loader_firebase_crashlytics_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_firebase_crashlytics_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], FirebaseCrashlyticsPage);



/***/ }),

/***/ 400:
/*!*****************************************************************************!*\
  !*** ./src/app/modules/firebase-crashlytics/firebase-crashlytics.page.scss ***!
  \*****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJmaXJlYmFzZS1jcmFzaGx5dGljcy5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ 9232:
/*!*******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/firebase-crashlytics/firebase-crashlytics.page.html ***!
  \*******************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button defaultHref=\"home\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title>Firebase Crashlytics</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-card>\r\n    <ion-card-header>\r\n      <ion-card-title>About</ion-card-title>\r\n    </ion-card-header>\r\n    <ion-card-content>\r\n      ⚡️ Capacitor plugin for Firebase Crashlytics.\r\n    </ion-card-content>\r\n    <ion-row class=\"ion-no-padding\">\r\n      <ion-col>\r\n        <ion-button\r\n          fill=\"clear\"\r\n          (click)=\"openOnGithub()\"\r\n          class=\"ion-float-right\"\r\n          >GitHub</ion-button\r\n        >\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-card>\r\n  <ion-card>\r\n    <ion-card-header>\r\n      <ion-card-title>Demo</ion-card-title>\r\n    </ion-card-header>\r\n    <ion-card-content>\r\n      <ion-button (click)=\"crash()\">Crash</ion-button>\r\n    </ion-card-content>\r\n  </ion-card>\r\n</ion-content>\r\n");

/***/ })

}]);
//# sourceMappingURL=src_app_modules_firebase-crashlytics_firebase-crashlytics_module_ts.js.map