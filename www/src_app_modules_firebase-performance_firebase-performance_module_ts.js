(self["webpackChunkcapacitor_firebase_plugin_demo"] = self["webpackChunkcapacitor_firebase_plugin_demo"] || []).push([["src_app_modules_firebase-performance_firebase-performance_module_ts"],{

/***/ 6391:
/*!**************************************************************************************!*\
  !*** ./node_modules/@capacitor-community/firebase-analytics/dist/esm/definitions.js ***!
  \**************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);

//# sourceMappingURL=definitions.js.map

/***/ }),

/***/ 4436:
/*!********************************************************************************!*\
  !*** ./node_modules/@capacitor-community/firebase-analytics/dist/esm/index.js ***!
  \********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FirebaseAnalytics": () => (/* binding */ FirebaseAnalytics)
/* harmony export */ });
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @capacitor/core */ 8384);
/* harmony import */ var _definitions__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./definitions */ 6391);

const FirebaseAnalytics = (0,_capacitor_core__WEBPACK_IMPORTED_MODULE_0__.registerPlugin)("FirebaseAnalytics", {
    web: () => __webpack_require__.e(/*! import() */ "node_modules_capacitor-community_firebase-analytics_dist_esm_web_js").then(__webpack_require__.bind(__webpack_require__, /*! ./web */ 1783)).then((m) => new m.FirebaseAnalyticsWeb()),
});


//# sourceMappingURL=index.js.map

/***/ }),

/***/ 8438:
/*!****************************************************************************************!*\
  !*** ./node_modules/@robingenz/capacitor-firebase-performance/dist/esm/definitions.js ***!
  \****************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);

//# sourceMappingURL=definitions.js.map

/***/ }),

/***/ 1798:
/*!**********************************************************************************!*\
  !*** ./node_modules/@robingenz/capacitor-firebase-performance/dist/esm/index.js ***!
  \**********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FirebasePerformance": () => (/* binding */ FirebasePerformance)
/* harmony export */ });
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @capacitor/core */ 8384);
/* harmony import */ var _definitions__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./definitions */ 8438);

const FirebasePerformance = (0,_capacitor_core__WEBPACK_IMPORTED_MODULE_0__.registerPlugin)('FirebasePerformance', {
    web: () => __webpack_require__.e(/*! import() */ "node_modules_robingenz_capacitor-firebase-performance_dist_esm_web_js").then(__webpack_require__.bind(__webpack_require__, /*! ./web */ 8726)).then(m => new m.FirebasePerformanceWeb()),
});


//# sourceMappingURL=index.js.map

/***/ }),

/***/ 5814:
/*!*************************************************************************************!*\
  !*** ./src/app/modules/firebase-performance/firebase-performance-routing.module.ts ***!
  \*************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FirebasePerformancePageRoutingModule": () => (/* binding */ FirebasePerformancePageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _firebase_performance_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./firebase-performance.page */ 6570);




const routes = [
    {
        path: '',
        component: _firebase_performance_page__WEBPACK_IMPORTED_MODULE_0__.FirebasePerformancePage,
    },
];
let FirebasePerformancePageRoutingModule = class FirebasePerformancePageRoutingModule {
};
FirebasePerformancePageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], FirebasePerformancePageRoutingModule);



/***/ }),

/***/ 7396:
/*!*****************************************************************************!*\
  !*** ./src/app/modules/firebase-performance/firebase-performance.module.ts ***!
  \*****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FirebasePerformancePageModule": () => (/* binding */ FirebasePerformancePageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _app_shared__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @app/shared */ 1679);
/* harmony import */ var _firebase_performance_routing_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./firebase-performance-routing.module */ 5814);
/* harmony import */ var _firebase_performance_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./firebase-performance.page */ 6570);





let FirebasePerformancePageModule = class FirebasePerformancePageModule {
};
FirebasePerformancePageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        imports: [_app_shared__WEBPACK_IMPORTED_MODULE_0__.SharedModule, _firebase_performance_routing_module__WEBPACK_IMPORTED_MODULE_1__.FirebasePerformancePageRoutingModule],
        declarations: [_firebase_performance_page__WEBPACK_IMPORTED_MODULE_2__.FirebasePerformancePage],
    })
], FirebasePerformancePageModule);



/***/ }),

/***/ 6570:
/*!***************************************************************************!*\
  !*** ./src/app/modules/firebase-performance/firebase-performance.page.ts ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FirebasePerformancePage": () => (/* binding */ FirebasePerformancePage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_firebase_performance_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./firebase-performance.page.html */ 4786);
/* harmony import */ var _firebase_performance_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./firebase-performance.page.scss */ 1719);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _robingenz_capacitor_firebase_performance__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @robingenz/capacitor-firebase-performance */ 1798);
/* harmony import */ var _capacitor_community_firebase_analytics__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @capacitor-community/firebase-analytics */ 4436);






let FirebasePerformancePage = class FirebasePerformancePage {
    constructor() {
        this.githubUrl = 'https://github.com/robingenz/capacitor-firebase-performance';
    }
    openOnGithub() {
        _capacitor_community_firebase_analytics__WEBPACK_IMPORTED_MODULE_3__.FirebaseAnalytics.initializeFirebase({
            apiKey: "AIzaSyBROjwWc9w-bIO_cEoHr1hKslPgJX0B-gM",
            authDomain: "hybrid-test-846be.firebaseapp.com",
            projectId: "hybrid-test-846be",
            storageBucket: "hybrid-test-846be.appspot.com",
            messagingSenderId: "150037174770",
            appId: "1:150037174770:web:9f9f4c607d38fc972b736b",
            measurementId: "G-NRWFB89SGS"
        });
        _capacitor_community_firebase_analytics__WEBPACK_IMPORTED_MODULE_3__.FirebaseAnalytics.getAppInstanceId();
        _capacitor_community_firebase_analytics__WEBPACK_IMPORTED_MODULE_3__.FirebaseAnalytics.setUserId({
            userId: "000bravvo",
        });
        _capacitor_community_firebase_analytics__WEBPACK_IMPORTED_MODULE_3__.FirebaseAnalytics.setUserProperty({
            name: "test",
            value: "heal",
        });
    }
    startTrace() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            yield _robingenz_capacitor_firebase_performance__WEBPACK_IMPORTED_MODULE_2__.FirebasePerformance.startTrace({ traceName: 'test_trace' });
        });
    }
    stopTrace() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            yield _robingenz_capacitor_firebase_performance__WEBPACK_IMPORTED_MODULE_2__.FirebasePerformance.stopTrace({ traceName: 'test_trace' });
        });
    }
    incrementMetric() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            yield _robingenz_capacitor_firebase_performance__WEBPACK_IMPORTED_MODULE_2__.FirebasePerformance.incrementMetric({
                traceName: 'test_trace',
                metricName: 'item_cache_hit',
                incrementBy: 1,
            });
        });
    }
};
FirebasePerformancePage.ctorParameters = () => [];
FirebasePerformancePage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-firebase-performance',
        template: _raw_loader_firebase_performance_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_firebase_performance_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], FirebasePerformancePage);



/***/ }),

/***/ 1719:
/*!*****************************************************************************!*\
  !*** ./src/app/modules/firebase-performance/firebase-performance.page.scss ***!
  \*****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJmaXJlYmFzZS1wZXJmb3JtYW5jZS5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ 4786:
/*!*******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/firebase-performance/firebase-performance.page.html ***!
  \*******************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button defaultHref=\"home\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title>Firebase Performance</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-card>\r\n    <ion-card-header>\r\n      <ion-card-title>About</ion-card-title>\r\n    </ion-card-header>\r\n    <ion-card-content>\r\n      ⚡️ Capacitor plugin for Firebase Performance Monitoring.\r\n    </ion-card-content>\r\n    <ion-row class=\"ion-no-padding\">\r\n      <ion-col>\r\n        <ion-button\r\n          fill=\"clear\"\r\n          (click)=\"openOnGithub()\"\r\n          class=\"ion-float-right\"\r\n          >GitHub</ion-button\r\n        >\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-card>\r\n  <ion-card>\r\n    <ion-card-header>\r\n      <ion-card-title>Demo</ion-card-title>\r\n    </ion-card-header>\r\n    <ion-card-content>\r\n      <ion-button (click)=\"startTrace()\">Start trace</ion-button>\r\n      <ion-button (click)=\"stopTrace()\">Stop trace</ion-button>\r\n      <ion-button (click)=\"incrementMetric()\">Increment metric</ion-button>\r\n    </ion-card-content>\r\n  </ion-card>\r\n</ion-content>\r\n");

/***/ })

}]);
//# sourceMappingURL=src_app_modules_firebase-performance_firebase-performance_module_ts.js.map